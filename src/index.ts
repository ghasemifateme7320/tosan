

import {initServer} from './server';

if (
    !process.env.LOG_PATH
) {
    throw new Error(
        'invalid environment variables, check .env.example to get more insight',
    );
}
initServer();

process.on('uncaughtException', (error) => {
    // tslint:disable:no-console
    console.error('Something bad happened here....');
    console.error(error);
    console.error(error.stack);
});

