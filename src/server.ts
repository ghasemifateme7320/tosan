import express, {Request, RequestHandler} from 'express';
import * as http from 'http';
import {initMongo} from "./db/mongo";
import {tosanRouter} from "./routes";

let server: http.Server;
const app = express();


export const initServer = (callback?: () => void) => {
    app.set('port', 3004);
    app.use((req:any, resp:any, next:any) => {
        next();
    });

    initMongo();
    app.use(tosanRouter);



    server = app.listen(app.get('port'), () => {
        // tslint:disable-next-line:no-console
        console.log(
            ` App is running at http://localhost:${app.get(
                'port',
            )} \n To read logs cd to project-main-directory and run: npm run log `,
        );
        // log.info(` App is running at http://localhost:${app.get('port')}`);
        if (callback) {
            callback();
        }
    });
};



