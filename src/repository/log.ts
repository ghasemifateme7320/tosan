import {logsMongooseSchema} from "../db/models/logModels";

export const findLogByTrackId = async (trackId:string)=>{
    return  logsMongooseSchema.findOne({trackId})
}

export const createLog = async (data: {
    ip: string,
    label: string,
    trackId: string,
    body?:string,
    query?:string,
    params?:string

}) => {
    const log =  await new logsMongooseSchema(data).save()
    return log
}

export const updateLog = async (trackId:string,data:{
    ip: string,
    label: string,
    trackId: string,
    body?:string,
    query?:string,
    params?:string
})=>{
    return logsMongooseSchema.findOneAndUpdate({trackId},data)
}


