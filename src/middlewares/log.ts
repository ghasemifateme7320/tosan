import {getFromRedis, setInRedis, setInRedisWithExpireTime} from "../db/redis";

const {uuid} = require('uuidv4');

export const addLogAndTrackId = async (req:any,res:any,next:any)=>{
    try {
        const trackId = uuid()
        await setInRedisWithExpireTime(trackId,{req},50000)
        res.locals.trackId = trackId
    }catch (e){
        getFromRedis()
    }
}