
import Router from 'express-promise-router';
import {send1} from "../controllers";
export const tosanRouter = Router();
const {uuid} = require('uuidv4');

tosanRouter.post('/snd1',
    async (request: any, response) => {
       const result =  send1({trackId:uuid(),query:request?.query,params:request?.params,body:request?.body})
        return response.send(result)

    });
