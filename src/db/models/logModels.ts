import {Document, model, Schema} from "mongoose"

export interface LogInterface {
    ip: string,
    label: string,
    trackId: string,
    body?:string,
    query?:string,
    params?:string
}


interface LogMongooseDocument extends LogInterface, Document {
}

const logsSchema = new Schema(
    {
        ip: {type: String, required: true},
        label: {type: String, required: true},
        trackId: {type: String, required: true},
        body: {type: String, required: false},
        query: {type: String, required: false},
        params: {type: String, required: false},
    }
)

export const logsMongooseSchema = model<LogMongooseDocument>('log', logsSchema)
