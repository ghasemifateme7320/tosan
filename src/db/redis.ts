import { createClient, RedisClient } from 'redis';

let redisClient: RedisClient | null;

export const getRedisClient = (): RedisClient => {
    const redisConfig: any = {
        host: process.env.REDIS_HOST,
        db: process.env.REDIS_DB || 0,
        port: Number(process.env.REDIS_PORT)
    };

    if (!redisClient) {
        redisClient = createClient(redisConfig);
        initClientListeners();
    }
    return redisClient;
};

export const closeRedisConnection = () => {
    if (redisClient) {
        redisClient.end(true);
        redisClient = null;
    }
};

export const getFromRedis = (key: string): Promise<any> => {
    return new Promise((resolve, reject) => {
        getRedisClient().get(key, (err, result) => {
            /* istanbul ignore if */
            if (err) {
                return reject(err);
            }
            if (!result || result ==="undefined") {
                return resolve(undefined);
            }
            try {
                return resolve(JSON.parse(result));

            }catch (e) {
                return resolve(result);
            }
        });
    });
};

export const setInRedis = (
    key: string,
    data: any,
    options?: any,
): Promise<any> => {
    return new Promise((resolve, reject) => {
        getRedisClient().set(key, JSON.stringify(data), (err, result) => {
            if (err) {
                return reject(err);
            }
            resolve(result);
        });
    });
};
export const setInRedisWithExpireTime = (
    key: string,
    data: any,
    expireTimeInSecond: number,
): Promise<any> => {
    return new Promise((resolve, reject) => {
        getRedisClient().setex(
            key,
            expireTimeInSecond,
            JSON.stringify(data),
            (err, result) => {
                if (err) {
                    return reject(err);
                }
                resolve(result);
            },
        );
    });
};

export const removeFromRedis = (key: string): Promise<any> => {
    return new Promise((resolve, reject) => {
        getRedisClient().del(key, (err, result) => {
            if (err) {
                return reject(err);
            }
            resolve(result);
        });
    });
};

const initClientListeners = () => {
    if (!redisClient) {
        return;
    }
    redisClient.on('error', (err) => {
        throw new Error('Error connecting to redis .. ' + err);
    });
};
