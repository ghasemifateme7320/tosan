import {connect, connection, ConnectionOptions} from "mongoose";

export const initMongo = async () => {
    let mongoUrl: string;
    const mongoConf: ConnectionOptions = {
        useNewUrlParser: true,
        autoReconnect: true,
        dbName: process.env.MONGO_DB_NAME,
    }

        mongoUrl = process.env.STANDALONE_URL as string;


    await connect(mongoUrl, mongoConf);
    const db = connection;
    db.on('error', (err) => {
        console.error('db connection error... ', err);
        throw  err
    });
    db.once('open', () => {
        console.debug('db opened...');
    });
};