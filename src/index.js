"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var server_1 = require("./server");
if (!process.env.LOG_PATH) {
    throw new Error('invalid environment variables, check .env.example to get more insight');
}
(0, server_1.initServer)();
process.on('uncaughtException', function (error) {
    // tslint:disable:no-console
    console.error('Something bad happened here....');
    console.error(error);
    console.error(error.stack);
});
