"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.initServer = void 0;
var express_1 = require("express");
var mongo_1 = require("./db/mongo");
var routes_1 = require("./routes");
var server;
var app = (0, express_1.default)();
var initServer = function (callback) {
    app.set('port', 3004);
    app.use(function (req, resp, next) {
        next();
    });
    (0, mongo_1.initMongo)();
    app.use(routes_1.tosanRouter);
    server = app.listen(app.get('port'), function () {
        // tslint:disable-next-line:no-console
        console.log(" App is running at http://localhost:".concat(app.get('port'), " \n To read logs cd to project-main-directory and run: npm run log "));
        // log.info(` App is running at http://localhost:${app.get('port')}`);
        if (callback) {
            callback();
        }
    });
};
exports.initServer = initServer;
